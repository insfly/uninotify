> UniNotify是为弥补UniMall在管理员通知上的不足，同时解决高频管理员通知问题。通过开放平台接纳所有需要通知场景。节约诸位开发成本。
 
#### 使用说明

UniNotify可以使用公共服务器（免费），也可以使用自己的公众号，自己的服务器自行部署。

#### 对接说明

1. 申请AppId和AppSecert。和大部分开放平台一样。由于现在未做自助申请通道，请编辑邮件携带，开通主体名，联系方式，回调URL（非必须，可轮训）。发送邮件到cz@iotechn.com 申请AppId和AppSecert

2. 联调开发

> 注意：所有接口均出现在 [http://public.dobbinsoft.com/info/](http://public.dobbinsoft.com/info/) 中，并且文档上带有JS前端签名，可以直接测试接口。统一调用地址为 [http://public.dobbinsoft.com/m.api](http://public.dobbinsoft.com/m.api)

2.1 签名算法。请求接口时需要对来源进行身份认证。详细签名过程：
    对请求参数的值加上AppSecert进行字符串排序。注意，此处仅对值进行排序。例如 a=cc&b=aa ， AppSecret为bb。则进行字符串排序的结果为 aabbcc。 然后将排完序的字符串进行URL编码，最后将URL编码后字符串进行SHA256不可逆加密。就得到签名了。
    
2.2 注册用户到开放平台。
    调用 developer.getRegisterUrl 可以获取到一个二维码URL。需直接将此链接转化为二维码。供用户微信扫描。用户扫码后即完成注册。此时开放平台系统回回调开发者系统，签名算法同上。其中这个接口中参数需要开发者提供的用户Id为开发者自己系统的用户Id。平台会由AppId + userId来唯一定位这个被推送的用户。
    
2.3 推送信息
    注册完用户后，可以向用户推送模板消息了。在此处提供预设推送和自定义推送的模板消息。需要开发者提供userId和推送内容。接口详见template组下的接口。若不理解，可参考Unimall中相关代码。
    
    